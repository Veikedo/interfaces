﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Xml;

namespace Lesson3
{
    public class Records : IEnumerable<Record>, IAlgorithm
    {
        private Record[] _records;
        public Records(XmlNode nodes)
        {
            _records = new Record[nodes.ChildNodes.Count];
            for (int i = 0; i < _records.Length; i++)
            {
                _records[i] = new Record(nodes.ChildNodes[i]);
            }
        }

        public IEnumerator<Record> GetEnumerator()
        {
            return new RecordsEnumerator(_records);
        }

        public object Search(string firstName = null, string phone = null)
        {
            try
            {
                if (phone != null)
                {
                    var records = _records.Where(record => record.Phone == phone);
                    if (records != null)
                        return Tuple.Create(records, "Записи найдены");
                }
                else
                {
                    var records = _records.Where(record => record.FirstName == firstName);
                    if (records != null)
                        return Tuple.Create(records, "Записи найдены");
                }
                return Tuple.Create<IEnumerable<Record>, string>(null, "Записи найдены");
            }
            catch
            {
                throw new NullReferenceException();
            }
        }

        public Record[] Sort()
        {
            var records = this._records;
            for (int i = 0; i < records.Length; i++)
            {
               for(int j = 0; j<records.Length-1-i; j++)
                {
                    if (records[j].Date.CompareTo(records[j+1].Date)==1)
                    {
                        Swap(j, j+1, ref records);
                    }
                }
            }
            return records;

        }

        private void Swap(int i, int j, ref Record[] records)
        {
            if (i<records.Length && j<records.Length)
            {
                var temp = records[i];
                records[i] = records[j];
                records[j] = temp;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
