﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lesson3
{
    public static class TimeConverterExtension
    {
        public static DateTime TimeConverter(this string time)
        {
            int hours, minutes, seconds;
            var timeSeparator = new char[] { ':', ' ', '.' };
            var timeMass = time.Split(timeSeparator).Select(item => Convert.ToInt32(item)).ToArray();
            if (timeMass.Length != 3)
                throw new FormatException("Неверный формат времени");
            hours= timeMass[0];
            minutes = timeMass[1];
            seconds = timeMass[2];
            return new DateTime(1, 1, 1, hours, minutes, seconds);
        }
    }
}
