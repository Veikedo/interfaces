﻿using System;
using System.Collections;
using System.Linq;
using System.Xml;

namespace Lesson3
{
    public class Record
    {
        public Record(XmlNode node)
        {
            FirstName = node["FirstName"].InnerText;
            LastName = node["LastName"].InnerText;
            Phone = node["Phone"].InnerText;
            Mail = node["Mail"].InnerText;
            ConvertTodateTime(node["RecordDate"].InnerText);
        }
        private void ConvertTodateTime(string datetime)
        {
            var splitMass = new char[] { ' ', 'T', 'Z' };
            var dateAndTime = datetime.Split(splitMass, StringSplitOptions.RemoveEmptyEntries);
            if (dateAndTime.Length != 2)
                throw new FormatException("Передан неверный формат даты");
            var date = dateAndTime[0].Split('.').Select(item=>Convert.ToInt32(item)).ToArray();
            Date = new DateTime(date[2], date[1], date[0]);
            Time = dateAndTime[1].TimeConverter();
        }


        public Record() { }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public DateTime Date { get; private set; }
        public string Phone { get; private set; }
        public string Mail { get; private set; }
        public DateTime Time { get; private set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName} {Date.ToString("dd.MM.yyyy")} {Time.ToString("HH.mm.ss")} {Phone} {Mail} ";
        }
    }
}
