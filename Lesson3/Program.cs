﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace Lesson3
{
    class Program
    {
        static void Main(string[] args)
        {
            var doc = new XmlDocument();
            doc.Load(@"C:\Projects\Lesson3\Lesson3\RecordList.xml");
            var nodes = doc.SelectSingleNode("RecordList");
            var records = new Records(nodes);
            foreach(var record in records)
            {
                Console.WriteLine(record.FirstName);
            }

            records.Sort();
            Console.WriteLine(string.Join("\n", records.ToList()));
            Console.ReadLine();
        }
    }


}
