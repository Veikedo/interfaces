﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Lesson3
{
    class RecordsEnumerator : IEnumerator<Record>
    {
        Record[] _records;
        private int position = -1;
        public RecordsEnumerator(Record[] records)
        {
            _records = records;
        }
        public Record Current
        {
            get
            {
                if (position < _records.Length)
                    return _records[position];
                return null;
            }
        }

        object IEnumerator.Current => Current;

        public void Dispose() { }

        public bool MoveNext()
        {
            position++;
            return (position < _records.Length);
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }
}
