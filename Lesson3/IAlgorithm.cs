﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson3
{
    public interface IAlgorithm
    {
        Record[] Sort();
        object Search(string firstName = null, string phone = null);
    }
}
